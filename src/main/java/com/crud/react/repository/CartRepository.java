package com.crud.react.repository;

import com.crud.react.model.Cart;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CartRepository  extends JpaRepository<Cart, Long> {

    @Query(value = "SELECT * FROM cart WHERE users_id= :usersId ", nativeQuery = true)
    Page<Cart> findAll(Long usersId, Pageable pageable);

    @Query(value = "SELECT * FROM cart WHERE users_id= :usersId ", nativeQuery = true)
    List<Cart> findAllProductDelete(Long usersId);

//    Page<Cart> findAll (Users usersid, Pageable pageable):
//Pengganti SELECT * FROM
}

package com.crud.react.model;
import javax.persistence.*;

@Entity
@Table(name = "product")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private  String name;

    @Lob
    @Column(name = "deskripsi")
    private  String deskripsi;

    @Lob
    @Column(name = "img")
    private String img;

    @Column(name= "harga")
    private  Long harga;

    public Product(String name, String deskripsi, String img, Long harga) {
        this.name = name;
        this.deskripsi = deskripsi;
        this.img = img;
        this.harga = harga;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", deskripsi='" + deskripsi + '\'' +
                ", img='" + img + '\'' +
                ", harga=" + harga +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public Long getHarga() {
        return harga;
    }

    public void setHarga(Long harga) {
        this.harga = harga;
    }

    public Product() {
    }
}

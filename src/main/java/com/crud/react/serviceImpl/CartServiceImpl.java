package com.crud.react.serviceImpl;

import com.crud.react.dto.CartDto;
import com.crud.react.exception.NotFoundException;
import com.crud.react.model.Cart;
import com.crud.react.model.Product;
import com.crud.react.repository.CartRepository;
import com.crud.react.repository.ProductRepository;
import com.crud.react.repository.UsersRepository;
import com.crud.react.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CartServiceImpl implements CartService {

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CartRepository cartRepository;

    @Override
    public Cart create(CartDto cart) {
        Product product = productRepository.findById(cart.getProductId()).orElseThrow(() -> new NotFoundException("Product id tidak ditemukan"));
        Cart create = new Cart();
        create.setQty(cart.getQty());
        create.setTotalPrice(product.getHarga() * cart.getQty());
        create.setUsersId(usersRepository.findById(cart.getUsersId()).orElseThrow(() -> new NotFoundException("User id tidak di temukan")));
        create.setProductId(product);
        return cartRepository.save(create);
    }

    @Override
    public Page<Cart> findAll(Long page, Long usersId) {
        Pageable pageable = PageRequest.of(Math.toIntExact(page), 5);
        return cartRepository.findAll(Long.valueOf(usersId),pageable);
    }

   @Override
   public Cart update(Long id, CartDto cart) {
        Cart data = cartRepository.findById(id).orElseThrow(() -> new NotFoundException("Id Tdiak ditemukan"));
        data.setQty(cart.getQty());
        data.setUsersId(usersRepository.findById(cart.getUsersId()).orElseThrow(() -> new NotFoundException("User tidak ditemukan ")));
        data.setProductId(productRepository.findById(cart.getProductId()).orElseThrow(() -> new NotFoundException("Product tidak ditemukan ")));
        return cartRepository.save(data);
   }

   @Override
   public Map<String, Object> delete(Long id) {
        cartRepository.deleteById(id);
        Map<String, Object> obj = new HashMap<>();
        obj.put("DELETE" , true);
        return obj;
   }

   @Override
    public Map<String, Boolean> deleteAll(Long usersId) {
       List<Cart> cartList = cartRepository.findAllProductDelete(usersId);
       cartRepository.deleteAll(cartList);
       Map<String, Boolean> obj = new HashMap<>();
       obj.put("DELETE", Boolean.TRUE);
       return obj;
   }
}

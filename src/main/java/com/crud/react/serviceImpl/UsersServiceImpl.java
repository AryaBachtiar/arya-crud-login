package com.crud.react.serviceImpl;

import com.crud.react.dto.LoginDto;
import com.crud.react.enumted.Role;
import com.crud.react.exception.InternalErrorException;
import com.crud.react.exception.NotFoundException;
import com.crud.react.jwt.JwtProvider;
import com.crud.react.model.Users;
import com.crud.react.repository.UsersRepository;
import com.crud.react.service.UsersService;
import com.google.auth.Credentials;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UsersServiceImpl implements UsersService {

    private static final String DOWNLOAD_URL = "https://firebasestorage.googleapis.com/v0/b/upload-image-example-8923d.appspot.com/o/%s?alt=media";
    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private ModelMapper modalmepper;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    UserDetailsService userDetailsService;
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtProvider jwtProvider;

    private String imageConverter(MultipartFile multipartFile) {
        try {
            String fileName = getExtensions(multipartFile.getOriginalFilename());
            File file = convertToFile(multipartFile, fileName);
            var RESPONSE_URL = uploadFile(file, fileName);
            file.delete();
            return RESPONSE_URL;
        } catch (Exception e) {
            e.getStackTrace();
            throw new InternalErrorException("error upload file");
        }
    }
    private String getExtensions(String fileName) {
        return fileName.split("\\.")[0];
    }

    private File convertToFile(MultipartFile multipartFile, String fileName) throws IOException {
        File file = new File(fileName);
        try (FileOutputStream fos = new FileOutputStream(file)) {
            fos.write(multipartFile.getBytes());
            fos.close();
        }
        return file;
    }

    private String uploadFile(File file, String fileName) throws IOException {
        BlobId blodId = BlobId.of("upload-image-example-8923d.appspot.com", fileName);
        BlobInfo blobInfo = BlobInfo.newBuilder(blodId).setContentType("media").build();
        Credentials credentials = GoogleCredentials.fromStream(new FileInputStream("./src/main/resources/serviceAccountKey.json"));
        Storage storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();
        storage.create(blobInfo, Files.readAllBytes(file.toPath()));
        return String.format(DOWNLOAD_URL, URLEncoder.encode(fileName, StandardCharsets.UTF_8));

    }

    private  String authories(String email, String password) {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
        } catch (BadCredentialsException e) {
            throw new InternalErrorException("Email Or Passwword");
        }
        UserDetails userDetails = userDetailsService.loadUserByUsername(email);
        return  jwtProvider.generateToken(userDetails);
    }

    @Override
    public Map<String, Object> login(LoginDto loginDto){
        String token = authories(loginDto.getEmail(), loginDto.getPassword());
        Users user = usersRepository.findByEmail(loginDto.getEmail()).get();
        Map<String, Object> response = new HashMap<>();
        response.put("Token", token);
        response.put("Expired", "15 menit");
        response.put("user", user);
        return response;
    }

    @Override
    public List<Users> getAllUsers() {
        return usersRepository.findAll();
    }

    @Override
    public Users addUsers(Users users, MultipartFile multipartFile) {
        String email = users.getEmail();
        String foto = imageConverter(multipartFile);
        users.setPassword(passwordEncoder.encode(users.getPassword()));
        users.setFoto(foto);
        if (users.getRole().name().equals("ADMIN"))
            users.setRole(Role.ADMIN);
        else  users.setRole(Role.USER);
        var validasi = usersRepository.findByEmail(email);
        if (validasi.isPresent()){
            throw new InternalErrorException("MAaaf");
        }
        return usersRepository.save(users);
    }

    @Override
    public Users getUsers(Long id) {
        var user = usersRepository.findById(id).orElseThrow(() -> new NotFoundException("Id Tidak Ditemukan"));
        try {
            user.setId(user.getId() + 0);
            return usersRepository.save(user);
        } catch (Exception y) {
            throw  new InternalErrorException("Kesalahan Munculkan Data");
        }
    }

    @Override
    public Users editUsers(Long id, Users users,  MultipartFile multipartFile) {
  Users users1= usersRepository.findById(id).get();
  String foto = imageConverter(multipartFile);
  users1.setNama(users.getNama());
  users1.setFoto(foto);
  users1.setAlamat(users.getAlamat());
  users1.setNomor(users.getNomor());
  users1.setDeskripsi(users.getDeskripsi());
        return usersRepository.save(users1);

    }

    @Override
    public void deleteUsersById(Long id) {

    }
}

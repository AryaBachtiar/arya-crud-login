package com.crud.react.serviceImpl;

import com.crud.react.exception.InternalErrorException;
import com.crud.react.exception.NotFoundException;
import com.crud.react.model.Product;
import com.crud.react.repository.ProductRepository;
import com.crud.react.service.ProductService;
import com.google.auth.Credentials;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

@Service
public class ProductServiceImpl implements ProductService {

    private static final String DOWNLOAD_URL = "https://firebasestorage.googleapis.com/v0/b/upload-image-example-8923d.appspot.com/o/%s?alt=media";
    @Autowired
    private ProductRepository productRepository;

    private String imageConverter(MultipartFile multipartFile) {
        try {
            String fileName = getExtensions(multipartFile.getOriginalFilename());
            File file = convertToFile(multipartFile, fileName);
            var RESPONSE_URL = uploadFile(file, fileName);
            file.delete();
            return RESPONSE_URL;
        } catch (Exception e) {
            e.getStackTrace();
            throw new InternalErrorException("error upload file");
        }
    }

    private String getExtensions(String fileName) {
        return fileName.split("\\.")[0];
    }

    private File convertToFile(MultipartFile multipartFile, String fileName) throws IOException {
        File file = new File(fileName);
        try (FileOutputStream fos = new FileOutputStream(file)) {
            fos.write(multipartFile.getBytes());
            fos.close();
        }
        return file;
    }

    private String uploadFile(File file, String fileName) throws IOException {
        BlobId blodId = BlobId.of("upload-image-example-8923d.appspot.com", fileName);
        BlobInfo blobInfo = BlobInfo.newBuilder(blodId).setContentType("media").build();
        Credentials credentials = GoogleCredentials.fromStream(new FileInputStream("./src/main/resources/serviceAccountKey.json"));
        Storage storage = StorageOptions.newBuilder().setCredentials(credentials).build().getService();
        storage.create(blobInfo, Files.readAllBytes(file.toPath()));
        return String.format(DOWNLOAD_URL, URLEncoder.encode(fileName, StandardCharsets.UTF_8));

    }

    @Override
    public Page<Product> getAllProduct(Long page, String search) {
        Pageable pageable = PageRequest.of(Math.toIntExact(page), 4);
        // Pageable di letakkan di bagian paling akhir di GetAll
        //PageRequest berfungsi agar daftar yang di tampilkan berapa banyak dalam 1 page
        return productRepository.findAll(search ,pageable);
    }

    @Override
    public Product getProduct(Long id) {
        return productRepository.findById(id).orElseThrow(() -> new NotFoundException("User id Not Found"));
    }

    @Override
    public Product editProduct(Long id, Product product,MultipartFile multipartFile) {
        Product products1 = productRepository.findById(id).get();
        String img = imageConverter(multipartFile);
        products1.setName(product.getName());
        products1.setDeskripsi(product.getDeskripsi());
        products1.setHarga(product.getHarga());
        products1.setImg(img);
        return productRepository.save(products1);
    }


    @Override
    public Product addProduct(Product product, MultipartFile multipartFile) {
        String img = imageConverter(multipartFile);
        Product product1 = new Product(product.getName(), product.getDeskripsi(), img, product.getHarga());
        return productRepository.save(product1);
    }

    @Override
    public void deleteProductById(Long id) {
        productRepository.deleteById(id);
    }
}

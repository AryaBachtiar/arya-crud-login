package com.crud.react.controller;

import com.crud.react.dto.LoginDto;
import com.crud.react.dto.ProfilDto;
import com.crud.react.dto.UserDto;
import com.crud.react.model.Users;
import com.crud.react.response.CommonResponse;
import com.crud.react.response.ResponseHelper;
import com.crud.react.service.UsersService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/user")
public class UsersController {

    @Autowired
    private UsersService usersService;

    @Autowired
    private ModelMapper modelMapper;

    @PostMapping("/sign-in")
    public CommonResponse<Map<String, Object>> login(@RequestBody LoginDto loginDto){
        return  ResponseHelper.ok(usersService.login((loginDto)));
    }

    @PostMapping(value = "/sign-up", consumes = "multipart/form-data")
    public CommonResponse<Users> addUsers( UserDto userDto, @RequestPart("foto")MultipartFile multipartFile){
        return  ResponseHelper.ok(usersService.addUsers(modelMapper.map(userDto, Users.class),multipartFile));
    }

    @GetMapping("/all")
    public CommonResponse<List<Users>> getAllUsers(){
        return ResponseHelper.ok(usersService.getAllUsers()) ;
    }

    @GetMapping("/{id}")
    public CommonResponse  getUsers(@PathVariable("id") Long id) {
        return ResponseHelper.ok(usersService.getUsers(id)) ;
    }
//    @PostMapping
//    public CommonResponse<Users> addUsers(@RequestBody Users users) {
//        return ResponseHelper.ok( usersService.addUsers(users)) ;
//    }
    @PutMapping(path = "/{id}", consumes = "multipart/form-data")
    public CommonResponse<Users> editUsersById( @PathVariable("id") Long id, ProfilDto profilDto, @RequestPart("file") MultipartFile multipartFile) {
        return ResponseHelper.ok(usersService.editUsers(id,modelMapper.map(profilDto, Users.class), multipartFile));
    }

    @DeleteMapping("/{id}")
    public void deleteUsersById(@PathVariable("id") Long id) { usersService.deleteUsersById(id);}
}


package com.crud.react.controller;

import com.crud.react.dto.CartDto;
import com.crud.react.model.Cart;
import com.crud.react.response.CommonResponse;
import com.crud.react.response.ResponseHelper;
import com.crud.react.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/cart")
public class CartController {

    @Autowired
    private CartService cartService;

    @PostMapping
    public CommonResponse<Cart> create (@RequestBody CartDto cart) {
        return ResponseHelper.ok( cartService.create(cart));
    }

    @GetMapping
    public CommonResponse<Page<Cart>> findAll (@RequestParam Long page,@RequestParam(required = false) Long usersId) {
        return ResponseHelper.ok (cartService.findAll(page, usersId));
    }
    @PutMapping("/{id}")
    public CommonResponse <Cart> update (@PathVariable("id") Long id, @RequestBody CartDto cart) {
        return ResponseHelper.ok (cartService.update(id, cart));
    }

    @DeleteMapping("/{id}")
    public CommonResponse<Map<String, Object>> delete(@PathVariable("id") Long id) {
        return ResponseHelper.ok(cartService.delete(id));
    }

    @DeleteMapping("/checkout")
    public CommonResponse<Map<String, Boolean>> deleteAll(@RequestParam(required = false) Long usersId) {
        return ResponseHelper.ok(cartService.deleteAll(usersId == null ? Long.valueOf("") : usersId));
    }
}

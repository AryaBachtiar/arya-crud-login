package com.crud.react.controller;

import com.crud.react.dto.ProductDto;
import com.crud.react.model.Product;
import com.crud.react.response.CommonResponse;
import com.crud.react.response.ResponseHelper;
import com.crud.react.service.ProductService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/product")
public class ProductController {
    @Autowired
    private ProductService productService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping("/all")
    public CommonResponse<Page<Product>> getAllProduct(@RequestParam(name = "page")Long page,
                                                       @RequestParam(required = false) String search)
//            Jika ingin menambahkan perintah harus menambahkan @RequestParam lagi
    {
        return ResponseHelper.ok(productService.getAllProduct(page,search== null ? "" : search));
    }

    @GetMapping("/{id}")
    public CommonResponse getProduct(@PathVariable("id") Long id) {
        return ResponseHelper.ok(productService.getProduct(id));
    }

    @PostMapping(consumes = "multipart/form-data")
    public CommonResponse<Product> addProduct(ProductDto productDto, @RequestPart("file") MultipartFile multipartFile) {
        return ResponseHelper.ok(productService.addProduct(modelMapper.map(productDto, Product.class
        ), multipartFile));
    }

    @PutMapping(path ="/{id}", consumes = "multipart/form-data")
    public CommonResponse<Product> editProductsById(@PathVariable("id") Long id, ProductDto productDto, @RequestPart("file") MultipartFile multipartFile) {
        return ResponseHelper.ok(productService.editProduct(id, modelMapper.map(productDto, Product.class), multipartFile));
    }

    @DeleteMapping("/{id}")
    public void deleteProductById(@PathVariable("id") Long id) {
        productService.deleteProductById(id);
    }
}


package com.crud.react.service;

import com.crud.react.dto.CartDto;
import com.crud.react.model.Cart;
import org.springframework.data.domain.Page;

import java.util.Map;

public interface CartService {
    Cart create(CartDto cart);

    Page<Cart> findAll (Long page, Long usersId);

    Cart update(Long id, CartDto cart);

    Map<String, Object> delete(Long id);
    Map<String, Boolean> deleteAll(Long usersId);
}

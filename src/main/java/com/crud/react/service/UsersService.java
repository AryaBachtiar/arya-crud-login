package com.crud.react.service;
import com.crud.react.dto.LoginDto;
import com.crud.react.model.Users;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

public interface UsersService {
    Map<String, Object> login(LoginDto loginDto);
    List<Users> getAllUsers();

    Users addUsers(Users users, MultipartFile multipartFile);


    Users getUsers(Long id);

    Users editUsers(Long id, Users users,  MultipartFile multipartFile);

    void deleteUsersById(Long id);
}

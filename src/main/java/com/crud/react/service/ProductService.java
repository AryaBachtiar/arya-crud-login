package com.crud.react.service;

import com.crud.react.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

public interface ProductService {
    Page<Product> getAllProduct(Long page, String search);
//Page digunakan untuk pengganti List

    Product getProduct(Long id);


    Product addProduct(Product product,MultipartFile multipartFile);


    Product editProduct(Long id, Product product,MultipartFile multipartFile);


    void deleteProductById(Long id);

}

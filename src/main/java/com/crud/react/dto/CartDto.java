package com.crud.react.dto;

public class CartDto {

    private Long productId;

    private Long usersId;

    private Long qty;

    public Long getProductId() {
        return productId;
    }
    public Long getUsersId() {
        return usersId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public void setUsersId(Long usersId) {
        this.usersId = usersId;
    }
    public Long getQty() {
        return qty;
    }
    public void setQty(Long qty) {
        this.qty = qty;
    }
}
